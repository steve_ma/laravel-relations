<?php
namespace Stevema\Relations;

class MorphOneToOne extends Relations {
    // 说明
    protected string $type = 'MorphOneToOne';
    protected string $typeComment = '多态一对一';

    // 说明 比如封面[Poster] 和 帖子[Post] 视频[Video] 的关系 这里可以是 'posters'
    protected ?string $comment = null;

    // 提供服务的主题 封面属于帖子和视频 这里就应该是封面 'poster' / Poster::class
    // 如果用 'poster' 别忘记把 'poster' 加入别名中 不然关系可能不生效
    protected ?string $subject_model = null;
    // 关联表名称 null => 当前类名 驼峰转 小写(下划线) 也可以自定义表名称
    protected ?string $relation_table_name = null;


    // 多态关联表中的able [poster] null => posterable [_type] [_id]
    protected ?string $able_name = null;
    protected ?string $able_id_name = null;
    protected ?string $able_type_name = null;

    // functionsName [ belongsTo belongsToMany morphTo morphToMany ]
    // 封面属于帖子和视频 这里就应该是Poster模型中的绑定方法名 'posterable' // Poster::find(1)->posterable
    protected ?string $toFunName = null;
    // functionsName [ hasOne hasMany hasOneThrough hasManyThrough morphOne morphMany morphedByMany ]
    // 封面属于帖子和视频 这里就应该是视频\帖子模型中的绑定方法名 'poster' // Post::find(1)->poster Video::find(1)->poster
    protected ?string $byFunName = null;

    // 用到关联的模型列表  'post/Post::class' => null, 'video/Video::class' => null 后面的null 是绑定的方法名  当然可以自定义
    protected array $relations = [
        // '别名/模型:class' => '方法名',
        // 'post' => null,
        // 'video' => null,
    ];
    // 是否默认模型
    // 当 belongsTo，hasOne，hasOneThrough 和 morphOne 这些关联方法返回 null 的时候
    // 你可以定义一个默认的模型返回。该模式通常被称为 空对象模式，它可以帮你省略代码中的一些条件判断。
    // 在下面这个例子中，如果 Post 模型中没有用户，那么 user 关联关系将会返回一个空的 App\Models\User 模型：
    // true => withDefault() array => withDefault($this->bywithDefault);
    // //暂时只支持 true 和 array()
    protected mixed $byWithDefault = true;

    public function initResolveRelationUsing():void {
        if($this->fails()){
            $modelClass = $this->subject_model;
            $morph = $this->toArray();
            if(class_exists($modelClass)) {
                $modelClass::resolveRelationUsing($morph['toFunName'], function ($model) use ($morph) {
                    return $model->morphTo(
                        $morph['able_name'], // able
                        $morph['able_type_name'], // able_type
                        $morph['able_id_name'], // able_id
                        $morph['primaries'][get_class($model)] // ownerKey  当前模型的主键
                    );
                });
            }
            foreach($this->relations as $ableClass => $ableName){
                if(class_exists($ableClass) && class_exists($modelClass)) {
                    $ableClass::resolveRelationUsing($ableName, function ($model) use ($morph) {
                        //$related, $name, $type = null, $id = null, $localKey = null
                        return $model->morphOne(
                            $morph['subject_model'],    // 关联模型
                            $morph['able_name'], // able
                            $morph['able_type_name'], // able_type
                            $morph['able_id_name'], // able_id
                            $morph['primarys'][get_class($model)] // localKey  当前模型的主键
                        );
                    });
                }
            }
            unset($modelClass);
            unset($morph);
        }
    }
    public function getUsed($arrs=[]):array {
        $modelClass = $this->subject_model;
        $morph = $this->toArray();

        if(class_exists($modelClass)) {
            $str = "";
            $str .= "  // {$morph['type']}[{$morph['typeComment']}] {$morph['comment']} \n";
            $str .= "  public function {$morph['toFunName']}(){ \n";
            $str .= "      return \$this->morphTo('{$morph['able_name']}', '{$morph['able_type_name']}', '{$morph['able_id_name']}', '{$morph['primaries'][$modelClass]}');\n";
            $str .= "  }\n";

            $arrs[$modelClass][] = $str;
        }
        foreach($this->relations as $ableClass => $ableName){
            if(class_exists($ableClass) && class_exists($modelClass)) {
                $str = "";
                $withDefaultStr = "";
                if($this->byWithDefault) {
                    if (is_array($morph['byWithDefault']) || is_callable($morph['byWithDefault'])) {
                        $withDefaultStr .= "->withDefault(" . $this->getValueSource($this->byWithDefault) . ")";
                    } else {
                        $withDefaultStr .= "->withDefault()";
                    }
                }
                $str .= "  // {$morph['type']}[{$morph['typeComment']}] {$morph['comment']} \n";
                $str .= "  public function {$ableName}(){ \n";
                $str .= "      return \$this->morphOne('{$morph['subject_model']}', '{$morph['able_name']}', '{$morph['able_type_name']}', '{$morph['able_id_name']}', '{$morph['primaries'][$ableClass]}'){$withDefaultStr}; \n";
                $str .= "  } \n";
                $arrs[$ableClass][] = $str;
            }
        }
        unset($modelClass);
        unset($morph);
        return $arrs;
    }
    public function initFuncNames($arrs = []):array {
        if($this->fails()){
            $modelClass = $this->subject_model;
            if(class_exists($modelClass)) {
                $arrs[$modelClass][] = [
                    'name' => $this->toFunName,
                    'type' => $this->type,
                    'typeComment' => $this->typeComment,
                    'comment' => $this->comment,
                    'master' => 'morphTo',
                ];
            }
            foreach($this->relations as $ableClass => $ableName){
                if(class_exists($ableClass) && class_exists($modelClass)) {
                    $arrs[$ableClass][] = [
                        'name' => $ableName,
                        'type' => $this->type,
                        'typeComment' => $this->typeComment,
                        'comment' => $this->comment,
                        'master' => 'morphOne',
                    ];
                }
            }
            unset($modelClass);
        }
        return $arrs;
    }
}
