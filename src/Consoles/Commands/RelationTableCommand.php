<?php


namespace Stevema\Relations\Consoles\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Stevema\MorphMap\MorphConfigResource;

class RelationTableCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'relation:table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '生成关系用到的数据库表文件';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var string $name */

        $config = app('config')->get('relations');

        $type = $this->choice('请选择一个关系', $config['relations'], 0);
        $this->line("您选择的是{$type}");
        $relation = new $type();
        if(!$relation->hasTable()){
            $this->error("您选择的关系不需要添加表");
            die;
        }
        if(!$relation->fails(1)){
            $this->error("您选择的关系配置有错误:".$relation->getError());
            die;
        }
        $will_path = database_path("migrations/2023_08_24_000001_create_{$relation->getTable()}_table.php");
        $stub_path = realpath(__DIR__ . '/../Stubs/'.$relation->getType().'.table.stub');
        if (!File::exists($will_path)) {
            if (!File::isDirectory(File::dirname($will_path))) {
                File::makeDirectory(File::dirname($will_path), 493, true);
            }
            File::put(
                $will_path,
                str_replace(
                    [
                        '{name}', '{comment}',
                        '{foreign_key}', '{related_key}',
                        '{able_type_name}', '{able_id_name}',
                    ],
                    [
                        $relation->getTable(), $relation->getComment(),
                        $relation->getForeignKey(), $relation->getRelatedKey(),
                        $relation->getAbleTypeName(),$relation->getAbleIdName(),
                    ],
                    File::get($stub_path)
                )
            );
            $this->components->info(sprintf('%s [%s] created successfully.', 'migrate', $will_path));
        } else {
            $this->components->error(sprintf('%s [%s] already exists.', 'migrate', $will_path));
        }

        return 0;

    }
}
