<?php
namespace Stevema\Relations\Consoles\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Stevema\Relations\Relations;

class MakeRelationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:relation {name : relation class name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '在app/Relations中创建关系模型';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var string $name */
        $name = $this->argument("name");
        $className = $name;
        $namespace = '';
        $nameArr = explode('/', $name);
        if(count($nameArr) > 1){
            $className = ucfirst(end($nameArr));
            $namespace = "\\".str_replace(['/'.$className, '/'], ['','\\'], $name);
        } else {
            $className = ucfirst($name);
        }
        $will_path = app_path("Relations/{$name}.php");
        $relations = new Relations();
        $arr = $relations->typeChooseLists();
        $type = $this->choice('请选择您的关系类型?', $arr, 0);
        $this->line("您选择的是{$type}");
        $type = $relations->getTypeByChoose($type);
        $stub_path = realpath(__DIR__ . '/../Stubs/'.$type.'.stub');
        if (!File::exists($will_path)) {
            if (!File::isDirectory(File::dirname($will_path))) {
                File::makeDirectory(File::dirname($will_path), 493, true);
            }
            File::put(
                $will_path,
                str_replace(
                    [
                        '{classname}', '{namespace}',
                    ],
                    [
                        $className, $namespace,
                    ],
                    File::get($stub_path)
                )
            );
            $this->components->info(sprintf('%s [%s] created successfully.', 'migrate', $will_path));
        } else {
            $this->components->error(sprintf('%s [%s] already exists.', 'migrate', $will_path));
        }

        return 0;
    }
}
