<?php


namespace Stevema\Relations\Consoles\Commands;

use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Stevema\MorphMap\MorphConfigResource;

class RelationUseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'relation:use {--a|all : 全部}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '查看关系使用方法';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** @var string $name */

        $config = app('config')->get('relations');
        $all = $this->option('all');

        $lines = [];
        $errors = [];
        if($all){
            foreach($config['relations'] as $type){
                $relation = new $type();
                if (!$relation->fails()) {
                    $errors[] = $type."配置有错误:".$relation->getError();
                } else {
                    $lines = $relation->getUsed($lines);
                }
            }
        } else {
            $type = $this->choice('请选择一个关系', $config['relations'], 0);
            $this->line("您选择的是{$type}");
            $relation = new $type();
            if (!$relation->fails()) {
                $errors[] = $type."配置有错误:".$relation->getError();
            } else {
                $lines = $relation->getUsed($lines);
            }
        }
        if(!empty($errors)){
            foreach($errors as $error){
                $this->error($error);
            }
        }
        if(!empty($lines)) {
            foreach ($lines as $class => $funcs) {
                $this->line($class);
                foreach ($funcs as $func) {
                    $this->line($func);
                }
            }
        }

        return 0;

    }
}
