<?php
namespace Stevema\Relations\Consoles\Commands;

use Illuminate\Console\Command;

class RelationListCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'relation:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '显示关系列表';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $config = app('config')->get('relations');
        $arr = [];
        $relations = $config['relations'];
        foreach($relations as $k => $v){
            $relation = new $v;
            $arr = $relation->initFuncNames($arr);
        }


        $headers = ['模型', '关联方法', '关系名称', '类型', '角色'];
        $orders = [];
        foreach($arr as $class => $relations){
            if(empty($relations) === FALSE) {
                foreach($relations as $key => $relation){
//                    'name' => $this->toFunName,
//                    'type' => $this->type,
//                    'typeComment' => $this->typeComment,
//                    'comment' => $this->comment,
//                    'master' => '提供服务',
                    $orders[] = [
                        $class, $relation['name'],
                        $relation['comment'],$relation['typeComment'],
                        $relation['master'],
                    ];
                }
            }
        }
        $this->table($headers, $orders);

        return 0;

    }
}
