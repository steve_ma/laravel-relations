<?php
namespace Stevema\Relations;

class ManyToMany extends Relations {
    // 说明
    protected string $type = 'ManyToMany';
    protected string $typeComment = '多对多';

    // 说明
    protected ?string $comment = null;

    // 提供服务的主题 模型或者模型的别名 null => 小写类名最后一节去掉最后的s
    protected ?string $subject_model = null;
    // 关联表名称 null => 小写类名最后一节
    protected ?string $relation_table_name = null;

    // 提供的服务模型 在附表或者关系表中的字段名 [User] => user_id
    protected ?string $foreign_key = null;

    // 关联模型在附表或者关系表中的字段名 [User] => user_id
    protected ?string $related_key = null;

    // functionsName [ belongsTo belongsToMany morphTo morphToMany ]
    protected ?string $toFunName = null;

    // functionsName [ hasOne hasMany hasOneThrough hasManyThrough morphOne morphMany morphedByMany ]
    protected ?string $byFunName = null;

    // 关系列表
    protected ?string $relation = null;
    // 默认情况下，pivot 对象只包含两个关联模型的主键，如果你的中间表里还有其他额外字段，你必须在定义关联时明确指出：
    protected array $withPivot = [];

    public function initResolveRelationUsing():void {
        if($this->fails()){
            $modelClass = $this->subject_model;
            $relationClass = $this->relation;
            $morph = $this->toArray();
//            dd($morph);
            if(class_exists($modelClass) && class_exists($relationClass)) {
                $modelClass::resolveRelationUsing($morph['toFunName'], function ($model) use ($morph) {
                    // $related, $table = null, $foreignPivotKey = null, $relatedPivotKey = null,
                    // $parentKey = null, $relatedKey = null, $relation = null
                    $return = $model->belongsToMany(
                        $morph['relation'], // related
                        $morph['relation_table_name'], // table
                        $morph['foreign_key'], // foreignPivotKey
                        $morph['related_key'], // relatedPivotKey
                        $morph['primaries'][get_class($model)], // parentKey
                        $morph['primaries'][$morph['relation']] , // relatedKey
//                        $morph['related_key'], // relation
                    );
                    if($morph['withPivot']){
                        $return->withPivot($morph['withPivot']);
                    }
                    return $return;
                });

                $relationClass::resolveRelationUsing($morph['byFunName'], function ($model) use ($morph) {
                    // $related, $table = null, $foreignPivotKey = null, $relatedPivotKey = null,
                    // $parentKey = null, $relatedKey = null, $relation = null
                    $return = $model->belongsToMany(
                        $morph['subject_model'], // related
                        $morph['relation_table_name'], // table
                        $morph['related_key'], // foreignPivotKey
                        $morph['foreign_key'], // relatedPivotKey
                        $morph['primaries'][get_class($model)], // parentKey
                        $morph['primaries'][$morph['subject_model']], // relatedKey
//                        $morph['related_key'], // relation
                    );
                    if($morph['withPivot']){
                        $return->withPivot($morph['withPivot']);
                    }
                    return $return;
                });
            }
            unset($modelClass);
            unset($relationClass);
            unset($morph);
        }
    }

    public function getUsed($arrs=[]):array {
        $modelClass = $this->subject_model;
        $relationClass = $this->relation;
        $morph = $this->toArray();
        if(class_exists($modelClass) && class_exists($relationClass)) {
            $str = "";
            $withPivotStr = "";
            if($this->withPivot){
                $withPivotStr .= "->withPivot(".$this->getValueSource($this->withPivot).")";
            }

            $str .= "  // {$morph['type']}[{$morph['typeComment']}] {$morph['comment']} \n";
            $str .= "  public function {$morph['toFunName']}(){ \n";
            $str .= "      return \$this->belongsToMany('{$relationClass}', '{$morph['relation_table_name']}', '{$morph['foreign_key']}', '{$morph['related_key']}', '{$morph['primaries'][$modelClass]}', '{$morph['primaries'][$relationClass]}'){$withPivotStr};\n";
            $str .= "  }\n";

            $arrs[$modelClass][] = $str;

            $str = "";
            $str .= "  // {$morph['type']}[{$morph['typeComment']}] {$morph['comment']} \n";
            $str .= "  public function {$morph['byFunName']}(){ \n";
            $str .= "      return \$this->belongsToMany('{$modelClass}', '{$morph['relation_table_name']}', '{$morph['related_key']}', '{$morph['foreign_key']}', '{$morph['primaries'][$relationClass]}', '{$morph['primaries'][$modelClass]}'){$withPivotStr};\n";
            $str .= "  }\n";
            $arrs[$relationClass][] = $str;
        }
        unset($modelClass);
        unset($relationClass);
        unset($morph);
        return $arrs;
    }
    public function initFuncNames($arrs = []):array {
        if($this->fails()){
            $modelClass = $this->subject_model;
            $relationClass = $this->relation;
//            dd($morph);
            if(class_exists($modelClass) && class_exists($relationClass)) {
                $arrs[$modelClass][] = [
                    'name' => $this->toFunName,
                    'type' => $this->type,
                    'typeComment' => $this->typeComment,
                    'comment' => $this->comment,
                    'master' => 'belongsToMany',
                ];
                $arrs[$relationClass][] = [
                    'name' => $this->byFunName,
                    'type' => $this->type,
                    'typeComment' => $this->typeComment,
                    'comment' => $this->comment,
                    'master' => 'belongsToMany',
                ];
            }
            unset($modelClass);
            unset($relationClass);
        }
        return $arrs;
    }

}
