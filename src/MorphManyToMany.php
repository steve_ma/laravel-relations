<?php
namespace Stevema\Relations;

class MorphManyToMany extends Relations {
    // 说明
    protected string $type = 'MorphManyToMany';
    protected string $typeComment = '多态多对多';

    // 说明 比如用户[User] 和 帖子[Post] 视频[Video] 的点赞[like]关系 这里可以是 'likes'
    protected ?string $comment = null;

    // 提供服务的主题  用户[User] 给 帖子[Post] 视频[Video] 的点赞[like] 这里写用户 'user/User::class'
    // 如果用 'user' 别忘记把 'user' 加入别名中 不然关系可能不生效
    protected ?string $subject_model = null;
    // 关联表名称 null => 当前类名 驼峰转 小写(下划线) 也可以自定义表名称
    protected ?string $relation_table_name = null;

    // 在附表或者关系表中的字段名 [User] => user_id
    protected ?string $foreign_key = null;

    // 多态关联表中的able [poster] null => likeable [_type] [_id]
    protected ?string $able_name = null;
    protected ?string $able_id_name = null;
    protected ?string $able_type_name = null;

    // functionsName [ belongsTo belongsToMany morphTo morphToMany ]
    // 提供服务的主题  用户[User] 给 帖子[Post] 视频[Video] 的点赞[like]
    // 这里给个默认值 就行 比如 likes  User::find(1)->post_likes User::find(1)->video_likes
    protected ?string $toFunName = null;
    // functionsName [ hasOne hasMany hasOneThrough hasManyThrough morphOne morphMany morphedByMany ]
    // 用户[User] 给 帖子[Post] 视频[Video] 的点赞[like] 这里就应该是视频\帖子模型中的绑定方法名 'likes' // Post::find(1)->likes Video::find(1)->likes
    protected ?string $byFunName = null;

    // 用到关联的模型列表  'post/Post::class' => null, 'video/Video::class' => null
    // 后面的null 是绑定的方法名  当然可以自定义 默认 模型_[toFunName]
    protected array $relations = [
        // '别名/模型:class' => '方法名',
        // 'post' => null,
        // 'video' => null,
    ];

    public function initResolveRelationUsing():void {
        if($this->fails()){
            $modelClass = $this->subject_model;
            $morph = $this->toArray();
            foreach($this->relations as $ableClass => $ableName){
                if(class_exists($ableClass) && class_exists($modelClass)) {
                    $ableClass::resolveRelationUsing($morph['byFunName'], function ($model) use ($morph, $modelClass) {
                        // $related, $name, $table = null, $foreignPivotKey = null,
                        // $relatedPivotKey = null, $parentKey = null, $relatedKey = null, $relation = null, $inverse = false
                        return $model->morphToMany(
                            $modelClass,    // 关联模型
                            $morph['able_name'], // 中间表的 able
                            $morph['relation_table_name'], // 中间表名
                            $morph['able_id_name'], // foreignPivotKey 中间表的 关联当前模型对应字段
                            $morph['foreign_key'], // relatedPivotKey 中间表的 主体键
                            $morph['primaries'][get_class($model)], // parentKey  当前模型的主键
                            $morph['primaries'][$modelClass] // relatedKey 关联模型的主键
                        );
                    });
                    $modelClass::resolveRelationUsing($ableName, function ($model) use ($morph, $ableClass) {
                        // $related, $name, $table = null, $foreignPivotKey = null,
                        // $relatedPivotKey = null, $parentKey = null, $relatedKey = null, $relation = null
                        return $model->morphedByMany(
                            $ableClass,
                            $morph['able_name'], // 中间表的 able
                            $morph['relation_table_name'], // 中间表名
                            $morph['foreign_key'], // foreignPivotKey 中间表的 关联当前模型对应字段
                            $morph['able_id_name'], // relatedPivotKey 中间表的 主体键
                            $morph['primaries'][get_class($model)], // parentKey  当前模型的主键
                            $morph['primaries'][$ableClass] // relatedKey 关联模型的主键
                        );
                    });
                }
            }
            unset($modelClass);
            unset($morph);
        } else {
            dd($this->error);
        }
    }

    public function getUsed($arrs=[]):array {
        $modelClass = $this->subject_model;
        $morph = $this->toArray();

        foreach($this->relations as $ableClass => $ableName){
            if(class_exists($ableClass) && class_exists($modelClass)) {
                $str = "";
                $str .= "  // {$morph['type']}[{$morph['typeComment']}] {$morph['comment']} \n";
                $str .= "  public function {$morph['byFunName']}(){ \n";
                $str .= "      return \$this->morphToMany('{$modelClass}', '{$morph['able_name']}', '{$morph['relation_table_name']}', '{$morph['able_id_name']}', '{$morph['foreign_key']}', '{$morph['primaries'][$ableClass]}', '{$morph['primaries'][$modelClass]}'); \n";
                $str .= "  } \n";
                $arrs[$ableClass][] = $str;

                $str = "";
                $str .= "  // {$morph['type']}[{$morph['typeComment']}] {$morph['comment']} \n";
                $str .= "  public function {$ableName}(){ \n";
                $str .= "      return \$this->morphedByMany('{$ableClass}', '{$morph['able_name']}', '{$morph['relation_table_name']}', '{$morph['foreign_key']}', '{$morph['able_id_name']}', '{$morph['primaries'][$modelClass]}', '{$morph['primaries'][$ableClass]}');\n";
                $str .= "  }\n";

                $arrs[$modelClass][] = $str;
            }
        }
        unset($modelClass);
        unset($morph);
        return $arrs;
    }
    public function initFuncNames($arrs = []):array {
        if($this->fails()){
            $modelClass = $this->subject_model;
            foreach($this->relations as $ableClass => $ableName){
                if(class_exists($ableClass) && class_exists($modelClass)) {
                    $arrs[$ableClass][] = [
                        'name' => $this->byFunName,
                        'type' => $this->type,
                        'typeComment' => $this->typeComment,
                        'comment' => $this->comment,
                        'master' => 'morphToMany',
                    ];
                    $arrs[$modelClass][] = [
                        'name' => $ableName,
                        'type' => $this->type,
                        'typeComment' => $this->typeComment,
                        'comment' => $this->comment,
                        'master' => 'morphedByMany',
                    ];
                }
            }
            unset($modelClass);
        }
        return $arrs;
    }
}
