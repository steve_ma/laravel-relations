<?php
namespace Stevema\Relations;

class OneToMany extends Relations {
    // 说明
    protected string $type = 'OneToMany';
    protected string $typeComment = '一对多';

    // 说明 比如用户[User] 和 帖子[Post] 的关系 这里可以是 'user_posts'
    protected ?string $comment = '';
    // 提供服务的模型 帖子属于用户 这里就应该是帖子 'post' / Post::class
    // 如果用 'post' 别忘记把 'post' 加入别名中 不然关系可能不生效
    protected ?string $subject_model = null;

    // 关联表名称 null => 小写类名最后一节
    protected ?string $relation_table_name = null;

    // 在附表或者关系表中的字段名 帖子属于用户 这里就应该是帖子表中个关联字段 'user_id'
    protected ?string $foreign_key = null;

    // functionsName [ belongsTo belongsToMany morphTo morphToMany ]
    // 帖子属于用户 这里就应该是帖子模型中的绑定方法名 'user' // Post::find(1)->user
    protected ?string $toFunName = null;
    // functionsName [ hasOne hasMany hasOneThrough hasManyThrough morphOne morphMany morphedByMany ]
    // 帖子属于用户 这里就应该是用户模型中的绑定方法名 'posts' // User::find(1)->posts
    protected ?string $byFunName = null;

    // 帖子属于用户 这里应该是 'user' / User::class
    // 如果用 'user' 别忘记把 'user' 加入别名中 不然关系可能不生效
    protected ?string $relation = null;
    // 是否默认模型
    // 当 belongsTo，hasOne，hasOneThrough 和 morphOne 这些关联方法返回 null 的时候
    // 你可以定义一个默认的模型返回。该模式通常被称为 空对象模式，它可以帮你省略代码中的一些条件判断。
    // 在下面这个例子中，如果 Post 模型中没有用户，那么 user 关联关系将会返回一个空的 App\Models\User 模型：
    // true => withDefault() array => withDefault($this->bywithDefault);
    // //暂时只支持 true 和 array()
    protected mixed $toWithDefault = true;

    public function initResolveRelationUsing():void {
        if($this->fails()){
            $modelClass = $this->subject_model;
            $relationClass = $this->relation;
            $morph = $this->toArray();
//            dd($morph);
            if(class_exists($modelClass) && class_exists($relationClass)) {
                $modelClass::resolveRelationUsing($morph['toFunName'], function ($model) use ($morph) {
                    // $related, $foreignKey = null, $ownerKey = null, $relation = null
                    $return = $model->belongsTo(
                        $morph['relation'], // related
                        $morph['related_key'], // foreignKey
                        $morph['primaries'][get_class($model)] // ownerKey  当前模型的主键
                    );
                    if($morph['toWithDefault']){
                        if(is_array($morph['toWithDefault']) || is_callable($morph['toWithDefault'])) {
                            $return->withDefault($morph['toWithDefault']);
                        } else {
                            $return->withDefault();
                        }
                    }
                    return $return;
                });
                $relationClass::resolveRelationUsing($morph['byFunName'], function ($model) use ($morph) {
                    //$related, $foreignKey = null, $localKey = null
                    return $model->hasMany(
                        $morph['subject_model'], // related
                        $morph['foreign_key'], // foreignKey
                        $morph['primaries'][get_class($model)] // localKey  当前模型的主键
                    );
                });
            }
            unset($modelClass);
            unset($relationClass);
            unset($morph);
        }
    }
    public function getUsed($arrs=[]):array {
        $modelClass = $this->subject_model;
        $relationClass = $this->relation;
        $morph = $this->toArray();
        if(class_exists($modelClass) && class_exists($relationClass)) {
            $str = "";
            $withDefaultStr = "";
            $str .= "  // {$morph['type']}[{$morph['typeComment']}] {$morph['comment']} \n";
            $str .= "  public function {$morph['toFunName']}(){ \n";
            if($this->toWithDefault){
                if(is_array($morph['toWithDefault']) || is_callable($morph['toWithDefault'])) {
                    $withDefaultStr .= "->withDefault(".$this->getValueSource($this->toWithDefault).")";
                } else {
                    $withDefaultStr .= "->withDefault()";
                }
            }
            $str .= "      return \$this->belongsTo('{$morph['relation']}', '{$morph['foreign_key']}', '{$morph['primaries'][$modelClass]}'){$withDefaultStr};\n";
            $str .= "  }\n";

            $arrs[$modelClass][] = $str;
            $str = "";
            $withDefaultStr = "";
            $str .= "  // {$morph['type']}[{$morph['typeComment']}] {$morph['comment']} \n";
            $str .= "  public function {$morph['byFunName']}(){ \n";
            $str .= "      return \$this->hasMany('{$morph['subject_model']}', '{$morph['foreign_key']}', '{$morph['primaries'][$relationClass]}'){$withDefaultStr}; \n";
            $str .= "  } \n";
            $arrs[$relationClass][] = $str;
        }
        unset($modelClass);
        unset($relationClass);
        unset($morph);
        return $arrs;
    }
    public function initFuncNames($arrs = []):array {
        if($this->fails()){
            $modelClass = $this->subject_model;
            $relationClass = $this->relation;
//            dd($morph);
            if(class_exists($modelClass) && class_exists($relationClass)) {
                $arrs[$modelClass][] = [
                    'name' => $this->toFunName,
                    'type' => $this->type,
                    'typeComment' => $this->typeComment,
                    'comment' => $this->comment,
                    'master' => 'belongsTo',
                ];
                $arrs[$relationClass][] = [
                    'name' => $this->byFunName,
                    'type' => $this->type,
                    'typeComment' => $this->typeComment,
                    'comment' => $this->comment,
                    'master' => 'hasMany',
                ];
            }
            unset($modelClass);
            unset($relationClass);
        }
        return $arrs;
    }
}
