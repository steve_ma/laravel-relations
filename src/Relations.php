<?php
namespace Stevema\Relations;

class Relations {
    // 关系配置
    public const ONETOONE = 'OneToOne';
    public const ONETOONE_COMMENT = '一对一';
    public const ONETOMANY = 'OneToMany';
    public const ONETOMANY_COMMENT = '一对多';
    public const MANYTOMANY = 'ManyToMany';
    public const MANYTOMANY_COMMENT = '多对多';
    public const ONETOONETHROUGH = 'OneToOneThrough';
    public const ONETOONETHROUGH_COMMENT = '远程一对一';
    public const ONETOMANYTHROUGH = 'OneToManyThrough';
    public const ONETOMANYTHROUGH_COMMENT = '远程一对多';
    public const MORPHONETOONE = 'MorphOneToOne';
    public const MORPHONETOONE_COMMENT = '多态一对一';
    public const MORPHONETOMANY = 'MorphOneToMany';
    public const MORPHONETOMANY_COMMENT = '多态一对多';
    public const MORPHMANYTOMANY = 'MorphManyToMany';
    public const MORPHMANYTOMANY_COMMENT = '多态多对多';

    // config 配置
    protected array $alias = [];
    protected array $primaries = [];
    // 其它配置
    protected string $type = '';
    protected string $typeComment = '';
    // 说明
    protected ?string $comment = '点赞';

    // 错误信息
    public string $error = '';
    public function setError($error): void {
        $this->error = $error;
    }
    public function getError(): string
    {
        return $this->error;
    }
    public function fails($noModel = 0): bool
    {
        if(!$this->checkType()){
            $this->setError("关系类型不存在");
            return false;
        }
        if($this->isMorph()){
            return $this->FullSelfMorph($noModel);
        } elseif($this->isThrough()) {
            return $this->FullSelfThrough($noModel);
        } else {
            return $this->FullSelf($noModel);
        }
    }


    // 提供服务的主题 模型或者模型的别名 null => 小写类名最后一节去掉最后的s
    protected ?string $subject_model = null;
    // 提供的服务名称 模型类名最后的部分
    protected ?string $subject_name = null;
    // 关联表名称 null => 小写类名最后一节
    protected ?string $relation_table_name = null;

    // 提供的服务模型 在附表或者关系表中的字段名 [User] => user_id
    protected ?string $foreign_key = null;

    // 关联模型在附表或者关系表中的字段名 [User] => user_id
    protected ?string $related_key = null;

    // 中间 模型或者模型的别名 null
    protected ?string $through_model = null;
    // 中间 模型类名最后的部分
    protected ?string $through_name = null;

    // 中间 模型 的字段名 [User] => user_id
    protected ?string $through_key = null;

    // 多态关联表中的able [like] null => likeable
    protected ?string $able_name = null;
    protected ?string $able_id_name = null;
    protected ?string $able_type_name = null;

    // functionsName [ belongsTo belongsToMany morphTo morphToMany ]
    protected ?string $toFunName = null;

    // functionsName [ hasOne hasMany hasOneThrough hasManyThrough morphOne morphMany morphedByMany ]
    protected ?string $byFunName = null;

    // 关系 多态 relations 其它 relation
    protected ?string $relation = null;
    protected array $relations = [
        // '别名/模型:class' => '方法名',
    ];
    // 是否默认模型
    // 当 belongsTo，hasOne，hasOneThrough 和 morphOne 这些关联方法返回 null 的时候
    // 你可以定义一个默认的模型返回。该模式通常被称为 空对象模式，它可以帮你省略代码中的一些条件判断。
    // 在下面这个例子中，如果 Post 模型中没有用户，那么 user 关联关系将会返回一个空的 App\Models\User 模型：
    // true => withDefault() array => withDefault($this->bywithDefault);
    // //暂时只支持 true 和 array()
    protected mixed $byWithDefault = null;
    protected mixed $toWithDefault = null;

    // 默认情况下，pivot 对象只包含两个关联模型的主键，如果你的中间表里还有其他额外字段，你必须在定义关联时明确指出：
    protected array $withPivot = [];

    public function getTable(): ?string
    {
        return $this->relation_table_name;
    }
    public function getType(): ?string
    {
        return $this->type;
    }
    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function getForeignKey(): ?string
    {
        return $this->foreign_key;
    }
    public function getRelatedKey(): ?string
    {
        return $this->related_key ;
    }
    public function getAbleTypeName(): ?string
    {
        return $this->able_type_name;
    }
    public function getAbleIdName(): ?string
    {
        return $this->able_id_name;
    }

    public function __construct()
    {
        $config = app('config')->get('relations');
        $this->alias = $config['alias'];

        if(!empty($config['primaries'])) {
            foreach ($config['primaries'] as $k => $v) {
                $this->primaries[$this->getClass($k)] = $v;
            }
        }

    }
    public function typeLists(): array
    {
        return [
            static::ONETOONE => static::ONETOONE_COMMENT,
            static::ONETOMANY => static::ONETOMANY_COMMENT,
            static::MANYTOMANY => static::MANYTOMANY_COMMENT,
            static::ONETOONETHROUGH => static::ONETOONETHROUGH_COMMENT,
            static::ONETOMANYTHROUGH => static::ONETOMANYTHROUGH_COMMENT,
            static::MORPHONETOONE => static::MORPHONETOONE_COMMENT,
            static::MORPHONETOMANY => static::MORPHONETOMANY_COMMENT,
            static::MORPHMANYTOMANY => static::MORPHMANYTOMANY_COMMENT,
        ];
    }
    public function typeChooseLists(): array
    {
        return [
            static::ONETOONE . "[" . static::ONETOONE_COMMENT . "]",
            static::ONETOMANY . "[" . static::ONETOMANY_COMMENT . "]",
            static::MANYTOMANY . "[" . static::MANYTOMANY_COMMENT . "]",
            static::ONETOONETHROUGH . "[" . static::ONETOONETHROUGH_COMMENT . "]",
            static::ONETOMANYTHROUGH . "[" . static::ONETOMANYTHROUGH_COMMENT . "]",
            static::MORPHONETOONE . "[" . static::MORPHONETOONE_COMMENT . "]",
            static::MORPHONETOMANY . "[" . static::MORPHONETOMANY_COMMENT . "]",
            static::MORPHMANYTOMANY . "[" . static::MORPHMANYTOMANY_COMMENT . "]",
        ];
        return $arr;
    }
    public function getTypeByChoose(string $type): string
    {
        $arr = explode("[", $type);
        return $arr[0];
    }

    public function checkType(): bool
    {
        if(in_array($this->type, [
            static::ONETOONE,
            static::ONETOMANY,
            static::MANYTOMANY,
            static::ONETOONETHROUGH,
            static::ONETOMANYTHROUGH,
            static::MORPHONETOONE,
            static::MORPHONETOMANY,
            static::MORPHMANYTOMANY,
        ])){
            return true;
        }
        return false;
    }

    public function hasTable(): bool
    {
        if(in_array($this->type, [
            static::ONETOONE,
            static::ONETOMANY,
            static::MANYTOMANY,
            static::MORPHONETOONE,
            static::MORPHONETOMANY,
            static::MORPHMANYTOMANY,
        ])){
            return true;
        }
        return false;
    }

    public function isMorph(): bool
    {
        if(in_array($this->type, [
            static::MORPHONETOONE,
            static::MORPHONETOMANY,
            static::MORPHMANYTOMANY,
        ])){
            return true;
        }
        return false;
    }
    public function isThrough(): bool
    {
        if(in_array($this->type, [
            static::ONETOONETHROUGH,
            static::ONETOMANYTHROUGH,
        ])){
            return true;
        }
        return false;
    }
    public function isManyToMany(): bool
    {
        if(in_array($this->type, [
            static::MANYTOMANY,
            static::MORPHMANYTOMANY,
        ])){
            return true;
        }
        return false;
    }

    public function getClass(?string $name): ?string{
        //检查别名里面有没有
        if(empty($name)) return null;
        if(isset($this->alias[$name])){
            return $this->alias[$name];
        }
        return $name;
    }

    public function getPrimaryKey($name): string {
        //检查别名里面有没有 primarys 还是关键词不让用
        $classname = $this->getClass($name);
        if(isset($this->primaries[$classname])){
            return $this->primaries[$classname];
        }
        return 'id';
    }

    public function checkModelClass($name): bool
    {
//        assert($name instanceof Model);
        return class_exists($name);
    }
    public function FullSelfThrough($noModel=0):bool{
        // 提供服务的主题 模型或者模型的别名 user => App\Models\User
        $this->subject_model = $this->getClass($this->subject_model);
        $this->primaries = [];
        // 提供服务的主题为空 则 整个关系不存在
        if(!$this->checkModelClass($this->subject_model)){
            $this->setError("{$this->subject_model}类没找到");
            if(!$noModel) {
                return false;
            }
        }
        // 提供服务的主题模型的主键
        $this->primaries[$this->subject_model] = $this->getPrimaryKey($this->subject_model);

        if(empty($this->subject_name)){
            $this->subject_name = strtolower(class_basename($this->subject_model));
        }
        // 在附表或者关系表中的字段名 [User] => user_id
        if(empty($this->foreign_key)){
            $this->foreign_key = $this->subject_name."_".$this->primaries[$this->subject_model];
        }

        if(!empty($this->relation)){
            $this->relation = $this->getClass($this->relation);
            // 模型的主键
            $this->primaries[$this->relation] = $this->getPrimaryKey($this->relation);
            if(empty($this->relation_name)){
                $this->relation_name = strtolower(class_basename($this->relation));
            }
            if(empty($this->related_key)){
                $this->related_key = $this->relation_name."_".$this->primaries[$this->relation];
            }
        } else {
            $this->setError(get_class($this)."的relation是空的");
            if(!$noModel) {
                return false;
            }
        }

        if(!empty($this->through_model)){
            $this->through_model = $this->getClass($this->through_model);
            // 模型的主键
            $this->primaries[$this->through_model] = $this->getPrimaryKey($this->through_model);

            if(empty($this->through_name)){
                $this->through_name = strtolower(class_basename($this->through_model));
            }
            if(empty($this->through_key)){
                $this->through_key = $this->through_name."_".$this->primaries[$this->through_model];
            }
        } else {
            $this->setError(get_class($this)."的relation是空的");
            if(!$noModel) {
                return false;
            }
        }

        if(empty($this->toFunName)){
            $this->toFunName = $this->relation_name;
        }
        if(empty($this->byFunName)){
            $this->byFunName = $this->subject_name;
        }

        return true;
    }
    public function FullSelf($noModel=0):bool{
        // 提供服务的主题 模型或者模型的别名 user => App\Models\User
        $this->subject_model = $this->getClass($this->subject_model);
        $this->primaries = [];
        // 提供服务的主题为空 则 整个关系不存在
        if(!$this->checkModelClass($this->subject_model)){
            $this->setError("{$this->subject_model}类没找到");
            if(!$noModel) {
                return false;
            }
        }

        if(empty($this->relation_table_name)){
            $this->relation_table_name = $this->convertToUnderscore(class_basename(get_class($this)));
        }
        if(empty($this->subject_model)){
            $this->subject_model = strtolower(class_basename(get_class($this)));
            if(str_ends_with($this->subject_model, 's')){
                $this->subject_model = substr($this->subject_model, 0, -1);
            }
        }
        // 提供服务的主题模型的主键
        $this->primaries[$this->subject_model] = $this->getPrimaryKey($this->subject_model);

        if(empty($this->subject_name)){
            $this->subject_name = strtolower(class_basename($this->subject_model));
        }
        // 在附表或者关系表中的字段名 [User] => user_id
        if(empty($this->foreign_key)){
            $this->foreign_key = $this->subject_name."_".$this->primaries[$this->subject_model];
        }

        if(!empty($this->relation)){
            $this->relation = $this->getClass($this->relation);
            // 模型的主键
            $this->primaries[$this->relation] = $this->getPrimaryKey($this->relation);

            if(empty($this->relation_name)){
                $this->relation_name = strtolower(class_basename($this->relation));
            }
            if(empty($this->related_key)){
                $this->related_key = $this->relation_name."_".$this->primaries[$this->relation];
            }
        } else {
            $this->setError(get_class($this)."的relation是空的");
            if(!$noModel) {
                return false;
            }
        }


        if(empty($this->toFunName)){
            $this->toFunName = $this->relation_name;
        }
        if(empty($this->byFunName)){
            $this->byFunName = $this->subject_name;
        }

        return true;
    }
    public function FullSelfMorph($noModel=0): bool
    {
        // 提供服务的主题 模型或者模型的别名 user => App\Models\User
        $this->subject_model = $this->getClass($this->subject_model);
        $this->primaries = [];
        // 提供服务的主题为空 则 整个关系不存在
        if(!$this->checkModelClass($this->subject_model)){
            $this->setError("{$this->subject_model}类没找到");
            if(!$noModel) {
                return false;
            }
        }

        if(empty($this->relation_table_name)){
            $this->relation_table_name = $this->convertToUnderscore(class_basename(get_class($this)));
        }
        if(empty($this->subject_model)){
            $this->subject_model = strtolower(class_basename(get_class($this)));
            if(str_ends_with($this->subject_model, 's')){
                $this->subject_model = substr($this->subject_model, 0, -1);
            }
        }
        // 提供服务的主题模型的主键
        $this->primaries[$this->subject_model] = $this->getPrimaryKey($this->subject_model);

        if(empty($this->subject_name)){
            $this->subject_name = strtolower(class_basename($this->subject_model));
        }
        // 在附表或者关系表中的字段名 [User] => user_id
        if(empty($this->foreign_key)){
            $this->foreign_key = $this->subject_name."_".$this->primaries[$this->subject_model];
        }
        // 多态关联表中的able [like] null => likeable
        if(empty($this->able_name)){
            $this->able_name = $this->subject_name."able";
        }
        if(empty($this->able_type_name)){
            $this->able_type_name = $this->able_name."_type";
        }
        if(empty($this->able_id_name)){
            $this->able_id_name = $this->able_name."_id";
        }
        if(empty($this->toFunName)){
            $this->toFunName = $this->subject_name."able";
        }
        if(empty($this->byFunName)){
            $this->byFunName = strtolower(class_basename(get_class($this)));
        }
        if(!empty($this->relations)){
            $relations = $this->relations;
            foreach($relations as $ableClass => $ableName){
                # 有用别名的情况下 先删除掉
                unset($this->relations[$ableClass]);
                # 再 重写
                $ableClass = $this->getClass($ableClass);
                $this->relations[$ableClass] = $ableName;
                if(empty($ableName)) {
                    if($this->isManyToMany()){
                        // 关系模型绑定 morph_name
                        $this->relations[$ableClass] = strtolower(class_basename($ableClass)) .'_'. $this->byFunName;
                    } else {
                        $this->relations[$ableClass] = $this->byFunName;
                    }
                }
                // 模型的主键
                $this->primaries[$ableClass] = $this->getPrimaryKey($ableClass);
            }
        } else {
            $this->setError(get_class($this)."的relations是空的");
            if(!$noModel) {
                return false;
            }
        }
        return true;
    }

    public function initResolveRelationUsing():void {
//        if($this->fails()){
//            // TODO
//        }
    }
    public function getUsed($arrs=[]):array {
        return $arrs;
    }
    public function initFuncNames($arrs = []):array {
//        if($this->fails()){
//            // TODO
//        }
        return $arrs;
    }

    public function toArray(): array
    {
        $arr = [];
        foreach($this as $k => $v){
            $arr[$k] = $v;
        }
        return $arr;
    }

    public function convertToUnderscore($string): string
    {
        $pattern = '/([a-z])([A-Z])/';

        $replacement = '${1}_${2}';

        return strtolower(preg_replace($pattern, $replacement, $string));
//        return strtolower(strtr($string, ' ', '_'));
//        return str_replace(' ', '_', ucwords(str_replace('_', ' ', $string)));
    }

    public function getValueSource($values){
        $str = "";
        if(is_array($values)){
            $str .= "[";
            $str .= "'".implode("','", $values)."'";
            $str .= "]";
        }
        return $str;
    }
}
