<?php

namespace Stevema\Relations;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class RelationsProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        // 发布配置文件
        $this->initPublishes();
        // 合并配置
        $this->mergeConfigFrom(
            realpath(__DIR__.'/Config/config.php'), 'relations'
        );
        // 注册命令
        $this->initCommands();
        $this->initUsingRelations();
    }
    protected function initPublishes(){
        // 发布配置文件
        if ($this->app->runningInConsole()) {
            $this->publishes([
                realpath(__DIR__ . '/Config/config.php') => config_path('relations.php'),
            ], 'relations');
        }
    }

    protected function initCommands(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                \Stevema\Relations\Consoles\Commands\MakeRelationCommand::class,
                \Stevema\Relations\Consoles\Commands\RelationListCommand::class,
                \Stevema\Relations\Consoles\Commands\RelationTableCommand::class,
                \Stevema\Relations\Consoles\Commands\RelationUseCommand::class,
            ]);
        }
    }

    public function initUsingRelations(): void {
        $config = app('config')->get('relations');

        // 动态注册模型别名
        Relation::enforceMorphMap($config['alias']);

        $relations = $config['relations'];
        foreach($relations as $k => $v){
            $relation = new $v;
            $relation->initResolveRelationUsing();
        }
    }
}
