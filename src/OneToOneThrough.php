<?php
namespace Stevema\Relations;

class OneToOneThrough extends Relations {
    // 说明
    protected string $type = 'OneToOneThrough';
    protected string $typeComment = '远程一对一';

    // 说明 比如维修师[Mechanic] 维修某一辆车[Car] 车会有车主 [Owner] 的关系 这里可以是 'mechanic_owner'
    // 表结构是  车主Owner 中有car_id字段  车Car 中有 mechanic_id 字段 Mechanic 中只有维修师的信息

    protected ?string $comment = '';
    // 维修师维修车主的车 这里应该是 'owner' / Owner::class
    // 如果用 'owner' 别忘记把 'owner' 加入别名中 不然关系可能不生效
    protected ?string $subject_model = null;

    // 维修师维修车主的车 这里应该是车主的车id字段 'car_id'
    protected ?string $foreign_key = null;

    // 中间 模型或者模型的别名 null 维修师维修车主的车 这里是 车的模型   'car' / Car::class
    // 如果用 'car' 别忘记把 'car' 加入别名中 不然关系可能不生效
    protected ?string $through_model = null;

    // 维修师维修车主的车 这里应该是车模型关联的维修师id字段名 'mechanic_id'
    protected ?string $through_key = null;

    // functionsName [ hasOne hasMany hasOneThrough hasManyThrough morphOne morphMany morphedByMany ]
    // 维修师维修车主的车 这里就应该是维修师模型中的绑定方法名 'car_owner' // Mechanic::find(1)->car_owner
    protected ?string $byFunName = null;

    // 提供服务的模型 维修师维修车主的车 这里就应该是维修师 'mechanic' / Mechanic::class
    // 如果用 'mechanic' 别忘记把 'mechanic' 加入别名中 不然关系可能不生效
    protected ?string $relation = null;

    // 是否默认模型
    // 当 belongsTo，hasOne，hasOneThrough 和 morphOne 这些关联方法返回 null 的时候
    // 你可以定义一个默认的模型返回。该模式通常被称为 空对象模式，它可以帮你省略代码中的一些条件判断。
    // 在下面这个例子中，如果 Mechanic 模型中没有车主，那么 Owner 关联 将会返回一个空的 App\Models\Owner 模型：
    // true => withDefault() array|callable => withDefault($this->withDefault);
    protected mixed $byWithDefault = true;

    public function initResolveRelationUsing():void {
        if($this->fails()){
            $modelClass = $this->subject_model;
            $throughModel = $this->through_model;
            $relationClass = $this->relation;
            $morph = $this->toArray();
//            dd($morph);
            if(class_exists($modelClass) && class_exists($throughModel)&& class_exists($relationClass)) {
                $relationClass::resolveRelationUsing($morph['byFunName'], function ($model) use ($morph) {
                    // $related, $through, $firstKey = null, $secondKey = null, $localKey = null, $secondLocalKey = null
                    $return = $model->hasOneThrough(
                        $morph['subject_model'], // related
                        $morph['through_model'], // through
                        $morph['through_key'], // firstKey 中间模型的外键名
                        $morph['foreign_key'], // secondKey 最终模型的外键名
                        $morph['primaries'][get_class($model)], // localKey  本地键名
                        $morph['primaries'][$morph['through_model']] // secondLocalKey  中间模型的本地键名
                    );
                    if($morph['byWithDefault']){
                        if(is_array($morph['byWithDefault']) || is_callable($morph['byWithDefault'])) {
                            $return->withDefault($morph['byWithDefault']);
                        } else {
                            $return->withDefault();
                        }
                    }
                    return $return;
                });
            }
            unset($modelClass);
            unset($relationClass);
            unset($morph);
        }
    }


    public function getUsed($arrs=[]):array {
        $modelClass = $this->subject_model;
        $throughModel = $this->through_model;
        $relationClass = $this->relation;
        $morph = $this->toArray();
        if(class_exists($modelClass) && class_exists($throughModel)&& class_exists($relationClass)) {
            $str = "";
            $withDefaultStr = "";
            if($this->byWithDefault) {
                if (is_array($morph['byWithDefault']) || is_callable($morph['byWithDefault'])) {
                    $withDefaultStr .= "->withDefault(" . $this->getValueSource($this->byWithDefault) . ")";
                } else {
                    $withDefaultStr .= "->withDefault()";
                }
            }

            $str .= "  // {$morph['type']}[{$morph['typeComment']}] {$morph['comment']} \n";
            $str .= "  public function {$morph['toFunName']}(){ \n";
            $str .= "      return \$this->hasOneThrough('{$relationClass}', '{$throughModel}', '{$morph['through_key']}', '{$morph['foreign_key']}', '{$morph['primaries'][$modelClass]}', '{$morph['primaries'][$throughModel]}');\n";
            $str .= "  }\n";

            $arrs[$modelClass][] = $str;
        }
        unset($modelClass);
        unset($throughModel);
        unset($relationClass);
        unset($morph);
        return $arrs;
    }
    public function initFuncNames($arrs = []):array {
        if($this->fails()){
            $modelClass = $this->subject_model;
            $throughModel = $this->through_model;
            $relationClass = $this->relation;
//            dd($morph);
            if(class_exists($modelClass) && class_exists($throughModel)&& class_exists($relationClass)) {
                $arrs[$relationClass][] = [
                    'name' => $this->byFunName,
                    'type' => $this->type,
                    'typeComment' => $this->typeComment,
                    'comment' => $this->comment,
                    'master' => 'hasOneThrough',
                ];
            }
            unset($modelClass);
            unset($relationClass);
        }
        return $arrs;
    }
}
